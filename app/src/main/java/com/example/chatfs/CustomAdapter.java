package com.example.chatfs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * адаптер для перерисовки списка сообщений
 */
public class CustomAdapter extends ArrayAdapter {
    private ArrayList<ListViewItem> objects;

    public CustomAdapter(Context context, int resource, ArrayList<ListViewItem> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }

    /**
     * количество разных типов элементов списка
     */
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /**
     * возвращаем один из двух вариантов виджета: item_green.xml / item_blue.xml
     * (зелёный фон / синий фон) в зависимости от того, наше ли это сообщение
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ListViewItem item = objects.get(position);

        if (item.getMyMessage()) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_green, null);
        } else {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_blue, null);
        }

        ((TextView)convertView.findViewById(R.id.text_datetime)).setText(item.getDateTime());
        ((TextView)convertView.findViewById(R.id.text_username)).setText(item.getUserName());
        ((TextView)convertView.findViewById(R.id.text_message)).setText(item.getMessage());

        return convertView;
    }
}