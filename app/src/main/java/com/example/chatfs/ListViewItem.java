package com.example.chatfs;

import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * элемент списка с полями одного сообщения чата
 */
public class ListViewItem {
    private SimpleDateFormat dateFormat;
    private QueryDocumentSnapshot snapshot;
    private boolean myMessage;

    /**
     * в конструктор передаём снапшот одного документа из БД с сырыми данными
     * также передаём признак - наше это сообщение или нет
     */
    ListViewItem(QueryDocumentSnapshot snapshot, boolean myMessage) {
        dateFormat = new SimpleDateFormat("MM.dd.yyyy HH:mm", new Locale("ru", "RU"));
        this.snapshot = snapshot;
        this.myMessage = myMessage;
    }

    /**
     * признак того, наше ли это сообщение
     */
    public boolean getMyMessage() {
        return myMessage;
    }

    /**
     * дата-время сообщения в виде строки
     */
    public String getDateTime() {
        Date dateTime = snapshot.getDate("dateTime");
        return null == dateTime ? "" : dateFormat.format(dateTime);
    }

    /**
     * имя пользователя в виде строки
     */
    public String getUserName() {
        String userName = snapshot.getString("userName");
        return null == userName ? "" : userName;
    }

    /**
     * текст сообщения в виде строки
     */
    public String getMessage() {
        String message = snapshot.getString("message");
        return null == message ? "" : message;
    }
}