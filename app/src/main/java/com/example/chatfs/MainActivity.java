package com.example.chatfs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity
        extends Activity
        implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener
{
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser user;
    private TextView textUsername;
    private Button buttonLogin;
    private Button buttonLogout;
    private Button buttonPost;
    private EditText editMessage;
    private ConstraintLayout layoutPost;
    private SwipeRefreshLayout swipeLayout;
    private ListView listviewChat;
    private ArrayList<ListViewItem> chatData;       //список сообщений
    private CustomAdapter chatAdapter;              //адаптер для перерисовки сообщений
    private int chatLimit = 5;                      //начальное количество сообщений на экране
    private static final int CHAT_LIMIT_INC = 5;    //сколько подгружать сообщений при свайпе
    private static final int RC_SIGN_IN = 1;
    private static final String DB_CHAT = "chat";
    public static final String TAG = "-fs-";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        textUsername = findViewById(R.id.text_username);
        buttonLogin = findViewById(R.id.button_login);
        buttonLogout = findViewById(R.id.button_logout);
        buttonPost = findViewById(R.id.button_post);
        editMessage = findViewById(R.id.edit_message);
        layoutPost = findViewById(R.id.layout_post);
        swipeLayout = findViewById(R.id.swipe_layout);
        listviewChat = findViewById(R.id.listview_chat);

        checkCurrentUser();

        buttonLogin.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
        buttonPost.setOnClickListener(this);
        swipeLayout.setOnRefreshListener(this);

        chatData = new ArrayList<>();
        chatAdapter = new CustomAdapter(this, R.id.layout_balloon, chatData);
        listviewChat.setAdapter(chatAdapter);

        /*
         * слушаем обновления в Firestore
         * при наступлении обновления переполучаем записи чата
         */
        db.collection(DB_CHAT)
                .orderBy("dateTime", Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed", e);
                            return;
                        }
                        if (null == snapshots) {
                            Log.w(TAG, "Listen failed - no snapshots");
                            return;
                        }
                        int num = 0;
                        chatData.clear();
                        for (QueryDocumentSnapshot document : snapshots) {
                            if (++num > chatLimit) break;
                            Log.d(TAG, "New: " + document.getId() + ", dt: " + document.getDate("dateTime"));
                            addChatItem(document);
                        }
                        chatAdapter.notifyDataSetChanged();
                        listviewChat.smoothScrollToPosition(chatAdapter.getCount() - 1);
                    }
                });
    }

    /**
     * если активность, инициирующая вход в аккаунт Firebase, завершилась успешно,
     * то проверяем статус текущего пользователя (залогинился или нет)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (RC_SIGN_IN == requestCode) {
            if (RESULT_OK == resultCode) {
                checkCurrentUser();
            } else {
                Log.d(TAG, "auth error");
            }
        }
    }

    /**
     * обрабатываем нажатия кнопок
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_login:
                login();
                break;
            case R.id.button_logout:
                logout();
                break;
            case R.id.button_post:
                post();
                break;
        }
    }

    /**
     * обрабатываем свайп: увеличиваем количество подгружаемых сообщений и перерисовываем чат
     */
    @Override
    public void onRefresh() {
        chatLimit += CHAT_LIMIT_INC;
        refreshMessages();
        swipeLayout.setRefreshing(false);
    }

    /**
     * получаем последние сообщения в количестве chatLimit и перерисовываем чат
     */
    private void refreshMessages() {
        db.collection(DB_CHAT)
                .orderBy("dateTime", Query.Direction.DESCENDING)
                .limit(chatLimit)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (null == task) {
                            Log.w(TAG, "Error getting documents");
                            return;
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Error getting documents", task.getException());
                            return;
                        }
                        chatData.clear();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, "Reload: " + document.getId() + ", dt: " + document.getDate("dateTime"));
                            addChatItem(document);
                        }
                        chatAdapter.notifyDataSetChanged();
                    }
                });
    }

    /**
     * инициируем вход в аккаунт Firebase с помощью одного из встроенных механизмов:
     * email, подтверждение по телефону, текущий google-аккаунт
     */
    private void login() {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.PhoneBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build()
        );

        startActivityForResult(
            AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build(),
            RC_SIGN_IN
        );
    }

    /**
     * выходим из аккаунта Firebase
     */
    private void logout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "Logged out");
                        updateAuthStatus(null);
                    }
                });
    }

    /**
     * добавляем в chatData одну запись из Firestore
     * также добавляем признак того, является ли это сообщение нашим (чтобы покрасить в нужный цвет)
     */
    private void addChatItem(QueryDocumentSnapshot document) {
        boolean myMessage = user != null && user.getUid().equals(document.getString("userId"));
        chatData.add(0, new ListViewItem(document, myMessage));
    }

    /**
     * записываем значения полей нового сообщения в chatItem
     * отправляем chatItem в Firestore
     */
    private void postMessage(String message) {
        if (null == user) return;

        HashMap<String, Object> chatItem = new HashMap<>();
        chatItem.put("userId", user.getUid());
        chatItem.put("dateTime", Timestamp.now());
        chatItem.put("userName", user.getDisplayName());
        chatItem.put("message", message);

        db.collection(DB_CHAT)
                .add(chatItem)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "Posted with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Posting error", e);
                    }
                });
    }

    /**
     * если в поле отправки сообщения есть текст, то отправляем сообщение в чат
     */
    private void post() {
        String message = editMessage.getText().toString();
        if (message.isEmpty()) return;
        postMessage(message);
        editMessage.setText("");
    }

    /**
     * проверяем статус пользователя и обновляем его имя в шапке
     * задаём видимость-невидимость кнопок логина-логаута и поля для отправки сообщений в чат
     * перерисовываем чат, чтобы перекрасить "наши" сообщения
     */
    private void updateAuthStatus(String username) {
        if (null == username) {
            textUsername.setText(getString(R.string.anonymous));
            buttonLogin.setVisibility(View.VISIBLE);
            buttonLogout.setVisibility(View.GONE);
            layoutPost.setVisibility(View.GONE);
            return;
        }
        textUsername.setText(username);
        buttonLogin.setVisibility(View.GONE);
        buttonLogout.setVisibility(View.VISIBLE);
        layoutPost.setVisibility(View.VISIBLE);
        refreshMessages();
    }

    /**
     * проверяем, залогинен ли текущий пользователь в аккаунт Firebase и обновляем статус
     */
    private void checkCurrentUser() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (null == user) {
            Log.d(TAG, "user = null");
            updateAuthStatus(null);
            return;
        }
        Log.d(TAG, "user = " + user.getUid() + " " + user.getDisplayName());
        updateAuthStatus(user.getDisplayName());
    }
}